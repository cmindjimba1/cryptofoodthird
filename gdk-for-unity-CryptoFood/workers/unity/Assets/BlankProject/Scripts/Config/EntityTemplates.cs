using Improbable;
using Improbable.Gdk.Core;
using Improbable.Gdk.PlayerLifecycle;
using Improbable.Gdk.QueryBasedInterest;
using Improbable.Gdk.TransformSynchronization;
using UnityEngine;

namespace BlankProject.Scripts.Config
{
    // EntityTemplates is where we are going to create game objects (food, items, etc.).
    // This is the default implementation.
    // Might decide to override it later on by using a class that implements IEntityGameObjectCreator (already done with GameObjectCreatorFromTransform) and pass
    // custom creator into EnableStandardGameObjectCreation method.
    public static class EntityTemplates
    {
        // Create a player entity.
        public static EntityTemplate CreatePlayerEntityTemplate(EntityId entityId, string workerId, byte[] serializedArguments)
        {
            var clientAttribute = EntityTemplate.GetWorkerAccessAttribute(workerId);
            var serverAttribute = UnityGameLogicConnector.WorkerType;

            var position = new Vector3(0, 1f, 0);
            var coords = Coordinates.FromUnityVector(position);

            var template = new EntityTemplate();
            template.AddComponent(new Position.Snapshot(coords), clientAttribute);
            template.AddComponent(new Metadata.Snapshot("Player"), serverAttribute);

            PlayerLifecycleHelper.AddPlayerLifecycleComponents(template, workerId, serverAttribute);

            // Player movement is client-authoritative
            TransformSynchronizationHelper.AddTransformSynchronizationComponents(template, clientAttribute, position);

            const int serverRadius = 500;
            var clientRadius = workerId.Contains(MobileClientWorkerConnector.WorkerType) ? 100 : 500;

            var serverQuery = InterestQuery.Query(Constraint.RelativeCylinder(serverRadius));
            var clientQuery = InterestQuery.Query(Constraint.RelativeCylinder(clientRadius));

            var interest = InterestTemplate.Create()
                .AddQueries<Metadata.Component>(serverQuery)
                .AddQueries<Position.Component>(clientQuery);
            template.AddComponent(interest.ToSnapshot(), serverAttribute);

            template.SetReadAccess(UnityClientConnector.WorkerType, MobileClientWorkerConnector.WorkerType, serverAttribute);
            template.SetComponentWriteAccess(EntityAcl.ComponentId, serverAttribute);

            return template;
        }

        // Was initially: public static EntityTemplate CreateSphereTemplate(Vector3 position = default)
        // The new one includes rotation and position.
        public static EntityTemplate CreateSphereTemplate(Quaternion rotation, Vector3 position = default)
        {
            var serverAttribute = UnityGameLogicConnector.WorkerType;

            var template = new EntityTemplate();
            template.AddComponent(new Position.Snapshot(Coordinates.FromUnityVector(position)), serverAttribute);
            template.AddComponent(new Metadata.Snapshot("Sphere"), serverAttribute);
            template.AddComponent(new Persistence.Snapshot(), serverAttribute);

            const int serverRadius = 500;

            TransformSynchronizationHelper.AddTransformSynchronizationComponents(template, serverAttribute, rotation, position);

            var query = InterestQuery.Query(Constraint.RelativeCylinder(serverRadius));
            var interest = InterestTemplate.Create().AddQueries<Position.Component>(query);
            template.AddComponent(interest.ToSnapshot());

            template.SetReadAccess(UnityClientConnector.WorkerType, MobileClientWorkerConnector.WorkerType, serverAttribute);

            return template;
        }

        // Here we are going to create the rest of the game objects
        
    }
}
